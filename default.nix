let sources = import ./nix/sources.nix;
in { pkgs ? import sources.nixpkgs { } }:

let

  naersk = pkgs.callPackage sources.naersk { };

in naersk.buildPackage {
  root = pkgs.nix-gitignore.gitignoreSource [ ] ./.;
  nativeBuildInputs = with pkgs; [ pkg-config ];
  buildInputs = with pkgs; [ dbus ];
  doCheck = true;
}
