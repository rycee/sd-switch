# sd-switch

## About

A tool primarily built for [Home Manager][] to assist with reloading
and restarting systemd units.

[Home Manager]: https://github.com/nix-community/home-manager

## Bug Reports and Sending Patches

Bug reports and patches are managed through my [sourcehut public inbox][].

[sourcehut public inbox]: https://lists.sr.ht/~rycee/public-inbox

## Development

The intended development flow is to use a Nix Flake development shell,
e.g., using [Direnv](https://direnv.net/) or directly running

``` console
$ nix develop .
…
```

This will help set up a shell containing the necessary development
dependencies. This shell will also be populated with a few utilities,
which can be run directly in the terminal.

- `p-format` -- will format the project's Rust, and Nix code.

## License

[GNU General Public License v3.0 or later](https://spdx.org/licenses/GPL-3.0-or-later.html)
