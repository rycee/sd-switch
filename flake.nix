{
  description = "SD Switch";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    naersk = {
      url = "github:nix-community/naersk";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };

  outputs = { self, nixpkgs, flake-utils, naersk }:
    let systems = [ "aarch64-linux" "i686-linux" "x86_64-linux" ];
    in flake-utils.lib.eachSystem systems (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        naersk-lib = naersk.lib."${system}";

        package = naersk-lib.buildPackage {
          root = pkgs.nix-gitignore.gitignoreSource [ ] ./.;
          nativeBuildInputs = with pkgs; [ pkg-config ];
          buildInputs = with pkgs; [ dbus ];
          doCheck = true;
        };

        pFormat = pkgs.writeShellScriptBin "p-format" ''
          shopt -s globstar
          set -euo pipefail

          if [[ $# -gt 0 && $1 = "-c" ]]; then
            check="--check"
          else
            check=""
          fi

          PATH=${with pkgs; nixpkgs.lib.makeBinPath [ cargo rustfmt nixfmt ]}

          cargo-fmt $check
          nixfmt $check **/*.nix
        '';
      in {
        packages = {
          sd-switch = package;
          default = package;
          p-format = pFormat;
        };

        devShell = pkgs.mkShell {
          name = "sd-switch-dev-shell";
          nativeBuildInputs = with pkgs; [
            cargo
            cargo-audit
            cargo-license
            clippy
            glib.bin
            nixfmt
            package.buildInputs
            package.nativeBuildInputs
            pkg-config
            rust-analyzer
            rustc
            rustfmt

            pFormat
          ];
        };
      });
}
