mod error;
mod file_compare;
mod systemd;
mod unit_file;

use std::{
    collections::HashSet,
    path::{Path, PathBuf},
    rc::Rc,
    time::Duration,
};
use unit_file::UnitFile;

use anyhow::{Context, Result};

fn pretty_unit_names<I>(unit_names: I) -> String
where
    I: IntoIterator,
    I::Item: AsRef<String>,
{
    let mut str_vec = unit_names
        .into_iter()
        .map(|s| String::from(s.as_ref()))
        .collect::<Vec<_>>();
    str_vec.sort();
    str_vec.join(", ")
}

fn commatized<I>(strings: I) -> String
where
    I: IntoIterator,
    I::Item: ToString,
{
    let mut str_vec = strings
        .into_iter()
        .map(|s| s.to_string())
        .collect::<Vec<_>>();
    str_vec.sort();
    str_vec.join(", ")
}

fn is_unit_available(unit_path: &Path) -> bool {
    unit_path.exists()
        && !unit_path
            .canonicalize()
            .map(|p| p == Path::new("/dev/null"))
            .unwrap_or(true)
}

/// Given an active unit name this returns the actual unit file name. In the
/// case of a parameterized unit, e.g., `foo@bar.service` this function returns
/// `foo@.service`. For nonparameterized unit names it returns none.
fn parameterized_base_name(unit_name: &str) -> Option<String> {
    let res = unit_name.splitn(2, '@').collect::<Vec<_>>();
    match res[..] {
        [base_name, arg_and_suffix] => {
            let res = arg_and_suffix.rsplitn(2, '.').collect::<Vec<_>>();
            match res[..] {
                [suffix, arg] if !arg.is_empty() => Some(format!("{}@.{}", base_name, suffix)),
                _ => None,
            }
        }
        _ => None,
    }
}

/// Returns the file path of the given unit name within the given directory.
///
/// If no matching file is found then `None` is returned.
///
/// If the given unit name is a parameterized named then an exactly matching
/// file is returned, if it exists, otherwise the template file path is
/// returned.
fn find_unit_file_path(unit_directory: &Path, unit_name: &str) -> Option<PathBuf> {
    Some(unit_directory.join(&unit_name))
        .filter(|e| is_unit_available(e))
        .or_else(|| {
            parameterized_base_name(unit_name)
                .map(|n| unit_directory.join(n))
                .filter(|e| is_unit_available(e))
        })
}

/// A plan of unit actions needed to accomplish the switch.
#[derive(Debug)]
struct SwitchPlan {
    stop_units: HashSet<Rc<String>>,
    start_units: HashSet<Rc<String>>,
    reload_units: HashSet<Rc<String>>,
    restart_units: HashSet<Rc<String>>,
    keep_old_units: HashSet<Rc<String>>,
    unchanged_units: HashSet<Rc<String>>,
    start_unmanaged_targets: HashSet<Rc<String>>,
    active_unit_names: HashSet<String>,
}

fn build_switch_plan(
    old_dir: &Option<PathBuf>,
    new_dir: &Path,
    service_manager: &systemd::ServiceManager,
) -> Result<SwitchPlan> {
    let mut stop_units = HashSet::new();
    let mut start_units = HashSet::new();
    let mut reload_units = HashSet::new();
    let mut restart_units = HashSet::new();
    let mut keep_old_units = HashSet::new();
    let mut unchanged_units = HashSet::new();
    let mut start_unmanaged_targets = HashSet::new();
    let mut active_unit_names = HashSet::new();

    let active_units = service_manager
        .list_units_by_patterns(&["active", "activating"], &[])
        .context("Failed to list active and activating units")?;

    for active_unit in active_units {
        let new_unit_path_opt = find_unit_file_path(new_dir, &active_unit.name);
        let old_unit_path_opt = old_dir
            .as_ref()
            .and_then(|d| find_unit_file_path(d, &active_unit.name));

        active_unit_names.insert(String::from(&active_unit.name));

        if let Some(new_unit_path) = new_unit_path_opt {
            let new_unit_file = UnitFile::load(&new_unit_path).with_context(|| {
                format!("Failed load of new unit file {}", new_unit_path.display())
            })?;

            if let Some(old_unit_path) = old_unit_path_opt {
                let old_unit_file = UnitFile::load(&old_unit_path).with_context(|| {
                    format!("Failed load of old unit file {}", old_unit_path.display())
                })?;

                if new_unit_file.unit_type() == unit_file::UnitType::Target {
                    if !new_unit_file.refuse_manual_start()? {
                        start_units.insert(Rc::new(active_unit.name));
                    }
                } else if unit_file::unit_eq(&old_unit_file, &new_unit_file)? {
                    unchanged_units.insert(Rc::new(active_unit.name));
                } else {
                    match new_unit_file.switch_method()? {
                        unit_file::UnitSwitchMethod::Reload => {
                            reload_units.insert(Rc::new(active_unit.name));
                        }
                        unit_file::UnitSwitchMethod::Restart => {
                            restart_units.insert(Rc::new(active_unit.name));
                        }
                        unit_file::UnitSwitchMethod::StopStart => {
                            if service_manager
                                .unit_manager(&active_unit)
                                .refuse_manual_stop()?
                            {
                                keep_old_units.insert(Rc::new(active_unit.name));
                            } else if new_unit_file.refuse_manual_start()? {
                                stop_units.insert(Rc::new(active_unit.name));
                            } else {
                                let rc = Rc::new(active_unit.name);
                                stop_units.insert(Rc::clone(&rc));
                                start_units.insert(Rc::clone(&rc));
                            }
                        }
                        unit_file::UnitSwitchMethod::KeepOld => {
                            keep_old_units.insert(Rc::new(active_unit.name));
                        }
                    };
                }
            } else {
                let rc = Rc::new(active_unit.name);
                stop_units.insert(Rc::clone(&rc));
                start_units.insert(Rc::clone(&rc));
            }
        } else if old_unit_path_opt.is_some() {
            stop_units.insert(Rc::new(active_unit.name));
        } else if active_unit.name.ends_with(".target")
            && !service_manager
                .unit_manager(&active_unit)
                .refuse_manual_start()?
        {
            start_unmanaged_targets.insert(Rc::new(active_unit.name));
        }
    }

    Ok(SwitchPlan {
        stop_units,
        start_units,
        reload_units,
        restart_units,
        keep_old_units,
        unchanged_units,
        start_unmanaged_targets,
        active_unit_names,
    })
}

fn exec_pre_reload<F>(
    plan: &SwitchPlan,
    service_manager: &systemd::ServiceManager,
    job_handler: F,
    dry_run: bool,
    timeout: &Option<Duration>,
) -> Result<()>
where
    F: Fn(&str, &str) + Send + 'static,
{
    if !plan.stop_units.is_empty() {
        println!("Stopping units: {}", pretty_unit_names(&plan.stop_units));
        if !dry_run {
            let job_monitor = service_manager.monitor_jobs_init(&plan.stop_units, job_handler)?;

            for uf in plan.stop_units.iter() {
                service_manager
                    .stop_unit(&uf)
                    .with_context(|| format!("Failed to stop unit {}", uf))?;
            }

            if !service_manager.monitor_jobs_finish(&job_monitor, timeout)? {
                eprintln!("Timeout waiting for systemd jobs");
            }
        }
    }

    Ok(())
}

fn exec_reload(
    service_manager: &systemd::ServiceManager,
    dry_run: bool,
    verbose: bool,
) -> Result<()> {
    if !dry_run {
        if verbose {
            println!("Resetting failed units");
        }
        service_manager
            .reset_failed()
            .context("Failed to reset failed systemd units")?;

        if verbose {
            println!("Reloading systemd");
        }
        service_manager
            .daemon_reload()
            .context("Failed to reload systemd")?;
    }

    Ok(())
}

fn exec_post_reload<F>(
    plan: &SwitchPlan,
    service_manager: &systemd::ServiceManager,
    job_handler: F,
    dry_run: bool,
    verbose: bool,
    timeout: &Option<Duration>,
) -> Result<()>
where
    F: Fn(&str, &str) + Send + 'static,
{
    let job_monitor = if dry_run {
        None
    } else {
        // Collect all unit names that will produce a job.
        let all_unit_names = plan
            .reload_units
            .iter()
            .chain(plan.restart_units.iter())
            .chain(plan.start_units.iter())
            .chain(plan.start_unmanaged_targets.iter())
            .collect::<Vec<_>>();

        Some(service_manager.monitor_jobs_init(&all_unit_names, job_handler)?)
    };

    if !plan.reload_units.is_empty() {
        println!("Reloading units: {}", pretty_unit_names(&plan.reload_units));
        if !dry_run {
            for uf in plan.reload_units.iter() {
                service_manager
                    .reload_unit(&uf)
                    .with_context(|| format!("Failed to reload unit {}", uf))?;
            }
        }
    }

    if !plan.restart_units.is_empty() {
        println!(
            "Restarting units: {}",
            pretty_unit_names(&plan.restart_units)
        );
        if !dry_run {
            for uf in plan.restart_units.iter() {
                service_manager
                    .restart_unit(&uf)
                    .with_context(|| format!("Failed to restart unit {}", uf))?;
            }
        }
    }

    if !plan.keep_old_units.is_empty() {
        println!(
            "Keeping old units: {}",
            pretty_unit_names(&plan.keep_old_units)
        );
    }

    if !plan.unchanged_units.is_empty() && verbose {
        println!(
            "Keeping units: {}",
            pretty_unit_names(&plan.unchanged_units)
        );
    }

    if !plan.start_units.is_empty() {
        println!("Starting units: {}", pretty_unit_names(&plan.start_units));
        if !dry_run {
            for uf in plan.start_units.iter() {
                service_manager
                    .start_unit(&uf)
                    .with_context(|| format!("Failed to start unit {}", uf))?;
            }
        }
    }

    if !plan.start_unmanaged_targets.is_empty() {
        if verbose {
            println!(
                "Unmanaged targets to start: {}",
                commatized(&plan.start_unmanaged_targets)
            );
        }

        if !dry_run {
            for target in plan.start_unmanaged_targets.iter() {
                service_manager
                    .start_unit(&target)
                    .with_context(|| format!("Failed to start unit {}", target))?;
            }
        }
    }

    if let Some(jm) = job_monitor {
        if !service_manager.monitor_jobs_finish(&jm, timeout)? {
            eprintln!("Timeout waiting for systemd jobs");
        }
    }

    // Check result.
    let active_units_new = service_manager.list_units_by_patterns(&[], &[])?;

    for active_unit in active_units_new {
        if active_unit.active_state == "inactive" {
            continue;
        } else if !plan.active_unit_names.contains(&active_unit.name) {
            println!(
                "Started {} - {}",
                active_unit.name, active_unit.active_state
            );
        }
    }

    Ok(())
}

/// Performs a systemd unit "switch".
pub fn switch(
    old_dir: &Option<PathBuf>,
    new_dir: &Path,
    dry_run: bool,
    verbose: bool,
    timeout: &Option<Duration>,
) -> Result<()> {
    let service_manager = systemd::ServiceManager::new_session()?;

    if verbose {
        println!("Got D-Bus connection: {:?}", service_manager.unique_name());
    }

    let plan = build_switch_plan(old_dir, new_dir, &service_manager)
        .context("Failed to build switch plan")?;

    let job_handler = move |name: &str, state: &str| {
        if verbose {
            println!("Job for {} {}", name, state)
        }
    };

    exec_pre_reload(&plan, &service_manager, job_handler, dry_run, timeout)
        .context("Failed to perform pre-reload tasks")?;

    exec_reload(&service_manager, dry_run, verbose)?;

    exec_post_reload(
        &plan,
        &service_manager,
        job_handler,
        dry_run,
        verbose,
        timeout,
    )
    .context("Failed to perform post-reload tasks")?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn can_get_base_name_for_parameterized_unit() {
        assert_eq!(
            parameterized_base_name("foo@bar.service"),
            Some(String::from("foo@.service"))
        );
        assert_eq!(
            parameterized_base_name("foo@bar.baz.service"),
            Some(String::from("foo@.service"))
        );
    }

    #[test]
    fn no_base_name_for_nonparameterized_units() {
        assert_eq!(parameterized_base_name("foo@.service"), None);
        assert_eq!(parameterized_base_name("foo.service"), None);
        assert_eq!(parameterized_base_name("foo@barservice"), None);
    }
}
