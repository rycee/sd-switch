use std::{error, fmt, io};

#[derive(Debug)]
pub enum Error {
    DBus(dbus::Error),
    DBusArgTypeMismatch(dbus::arg::TypeMismatchError),
    IniParse(ini::ParseError),
    Io(io::Error),
    SdSwitch(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::DBus(ref err) => err.fmt(f),
            Error::DBusArgTypeMismatch(ref err) => err.fmt(f),
            Error::Io(ref err) => err.fmt(f),
            Error::IniParse(ref err) => err.fmt(f),
            Error::SdSwitch(ref err) => err.fmt(f),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            Error::DBus(err) => err.source(),
            Error::DBusArgTypeMismatch(err) => err.source(),
            Error::Io(err) => err.source(),
            Error::IniParse(err) => err.source(),
            Error::SdSwitch(_) => None,
        }
    }
}

impl From<dbus::Error> for Error {
    fn from(err: dbus::Error) -> Self {
        Error::DBus(err)
    }
}

impl From<dbus::arg::TypeMismatchError> for Error {
    fn from(err: dbus::arg::TypeMismatchError) -> Self {
        Error::DBusArgTypeMismatch(err)
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Error::Io(err)
    }
}

impl From<ini::Error> for Error {
    fn from(err: ini::Error) -> Self {
        match err {
            ini::Error::Io(inner_err) => Error::from(inner_err),
            ini::Error::Parse(inner_err) => Error::IniParse(inner_err),
        }
    }
}
