use ini::Ini;
use std::path::{Path, PathBuf};

use crate::error::Error;
use crate::file_compare;

/// A systemd unit file path and content.
pub struct UnitFile {
    path: PathBuf,
    config: Ini,
    overrides: Option<Ini>,
}

#[derive(Debug, PartialEq)]
pub enum UnitType {
    Automount,
    Device,
    Mount,
    Path,
    Scope,
    Service,
    Slice,
    Socket,
    Swap,
    Target,
    Timer,
}

/// How to switch a unit that exists in both the old and new generation.
#[derive(Debug, PartialEq)]
pub enum UnitSwitchMethod {
    /// Reload the new unit.
    Reload,
    /// Restart the new unit.
    Restart,
    /// Stop the old unit and start the new unit.
    StopStart,
    /// Leave the old unit running.
    KeepOld,
}

impl UnitFile {
    pub fn load(path: &Path) -> Result<Self, Error> {
        // Validate that the path has an OK file extension.
        unit_path_to_unit_type(path)?;

        let config = Ini::load_from_file(path)?;

        let overrides_path = overrides_file_path(path);

        let overrides = if overrides_path.exists() {
            let overrides = Ini::load_from_file(overrides_path)?;
            Some(overrides)
        } else {
            None
        };

        Ok(Self {
            path: path.to_owned(),
            config,
            overrides,
        })
    }

    pub fn path(&self) -> &Path {
        &self.path
    }

    pub fn unit_type(&self) -> UnitType {
        // The error case should not occur since the same call succeeded in the
        // `load` constructor.
        unit_path_to_unit_type(self.path()).expect("Unexpected error")
    }

    pub fn refuse_manual_start(&self) -> Result<bool, Error> {
        self.get_bool_option("Unit", "RefuseManualStart")
            .map(|r| r.unwrap_or(false))
    }

    /// Determine how the unit should be switched.
    ///
    /// The switch method can be set in the unit file, or the corresponding
    /// `overrides.conf` file, using
    ///
    /// ```ini
    /// [Unit]
    /// X-SwitchMethod = A
    /// ```
    ///
    /// where `A` is one of `reload`, `restart`, `stop-start`, and `keep-old`.
    /// See `UnitSwitchMethod` for a description of the different switch
    /// methods.
    ///
    /// If `X-SwitchMethod=` is not present then the legacy `Unit` options
    /// `X-ReloadIfChanged=`, `X-RestartIfChanged=`, and `X-StopIfChanged` will
    /// be used.
    ///
    /// If none of the above options are present then the switch method
    /// `UnitSwitchMethod::StopStart` is returned.
    pub fn switch_method(&self) -> Result<UnitSwitchMethod, Error> {
        let switch_method = self.get_option("Unit", "X-SwitchMethod");

        match switch_method {
            Some("reload") => Ok(UnitSwitchMethod::Reload),
            Some("restart") => Ok(UnitSwitchMethod::Restart),
            Some("stop-start") => Ok(UnitSwitchMethod::StopStart),
            Some("keep-old") => Ok(UnitSwitchMethod::KeepOld),
            Some(unknown) => Err(Error::SdSwitch(format!(
                "Unknown unit switch method \"{}\"",
                unknown
            ))),
            None => {
                // Check legacy options.
                let reload_if_changed = self.get_bool_option("Unit", "X-ReloadIfChanged")?;
                let restart_if_changed = self.get_bool_option("Unit", "X-RestartIfChanged")?;
                let stop_if_changed = self.get_bool_option("Unit", "X-StopIfChanged")?;
                let legacy_switch_method =
                    match (reload_if_changed, restart_if_changed, stop_if_changed) {
                        (Some(true), _, _) => Some(UnitSwitchMethod::Reload),
                        (_, Some(false), _) => Some(UnitSwitchMethod::KeepOld),
                        (_, _, Some(false)) => Some(UnitSwitchMethod::Restart),
                        (_, _, _) => None,
                    };

                // If a legacy setup is present then use it, otherwise use the
                // true default of stop-start.
                Ok(legacy_switch_method.unwrap_or(UnitSwitchMethod::StopStart))
            }
        }
    }

    /// Gets a named Boolean option with priority given to the override file.
    fn get_bool_option(&self, section: &str, key: &str) -> Result<Option<bool>, Error> {
        self.get_option(section, key).map(systemd_bool).transpose()
    }

    /// Gets a named option with priority given to the override file.
    fn get_option(&self, section: &str, key: &str) -> Option<&str> {
        self.overrides
            .as_ref()
            .and_then(|ini| ini.get_from(Some(section), key))
            .or_else(|| self.config.get_from(Some(section), key))
    }
}

fn unit_path_to_unit_type(unit_path: &Path) -> Result<UnitType, Error> {
    unit_path
        .extension()
        .and_then(|v| v.to_str())
        .and_then(|v| match v {
            "service" => Some(UnitType::Service),
            "socket" => Some(UnitType::Socket),
            "device" => Some(UnitType::Device),
            "mount" => Some(UnitType::Mount),
            "automount" => Some(UnitType::Automount),
            "swap" => Some(UnitType::Swap),
            "target" => Some(UnitType::Target),
            "path" => Some(UnitType::Path),
            "timer" => Some(UnitType::Timer),
            "slice" => Some(UnitType::Slice),
            "scope" => Some(UnitType::Scope),
            _ => None,
        })
        .ok_or_else(|| {
            Error::SdSwitch(format!(
                "Could not determine unit type from extension of {:?}",
                unit_path
            ))
        })
}

fn overrides_file_path(unit_path: &Path) -> PathBuf {
    let mut file_name = unit_path.file_name().expect("XXX").to_os_string();
    file_name.push(".d");

    let mut overrides_path = unit_path.with_file_name(file_name);
    overrides_path.push("overrides.conf");
    overrides_path
}

/// Converts a string to a Boolean using systemd's rules.
fn systemd_bool(value: &str) -> Result<bool, Error> {
    match value {
        "1" | "yes" | "true" | "on" => Ok(true),
        "0" | "no" | "false" | "off" => Ok(false),
        _ => Err(Error::SdSwitch(format!(
            "Invalid Boolean value \"{}\"",
            value
        ))),
    }
}

/// Determine whether the given unit files are the same.
///
/// Currently this is done using simple file comparison but in the future some
/// fancy structural comparison may be implemented.
pub fn unit_eq(u1: &UnitFile, u2: &UnitFile) -> std::io::Result<bool> {
    let unit_files_same = file_compare::file_eq(&u1.path(), &u2.path())?;

    let opath1 = overrides_file_path(u1.path());
    let opath2 = overrides_file_path(u2.path());
    let override_files_same = opath1.exists() == opath2.exists()
        && (!opath1.exists() || file_compare::file_eq(opath1, opath2)?);

    Ok(unit_files_same && override_files_same)
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_data_path(file_name: &str) -> PathBuf {
        PathBuf::from("testdata/unit_file").join(file_name)
    }

    fn switch_method_file_path(method: &str) -> PathBuf {
        test_data_path(&format!("switch-method-{}.service", method))
    }

    #[test]
    fn can_read_switch_method_reload() {
        let uf = UnitFile::load(&switch_method_file_path("reload")).unwrap();
        assert_eq!(uf.switch_method().unwrap(), UnitSwitchMethod::Reload);
    }

    #[test]
    fn can_read_switch_method_restart() {
        let uf = UnitFile::load(&switch_method_file_path("restart")).unwrap();
        assert_eq!(uf.switch_method().unwrap(), UnitSwitchMethod::Restart);
    }

    #[test]
    fn can_read_switch_method_stop_start() {
        let uf = UnitFile::load(&switch_method_file_path("stop-start")).unwrap();
        assert_eq!(uf.switch_method().unwrap(), UnitSwitchMethod::StopStart);
    }

    #[test]
    fn can_read_switch_method_keep_old() {
        let uf = UnitFile::load(&switch_method_file_path("keep-old")).unwrap();
        assert_eq!(uf.switch_method().unwrap(), UnitSwitchMethod::KeepOld);
    }

    #[test]
    fn invalid_switch_method_yields_error() {
        let uf = UnitFile::load(&switch_method_file_path("nonsense")).unwrap();
        assert_eq!(
            format!("{}", uf.switch_method().unwrap_err()),
            "Unknown unit switch method \"nonesense\""
        );
    }

    #[test]
    fn can_handle_legacy_reload() {
        let uf = UnitFile::load(&test_data_path("legacy-reload.service")).unwrap();
        assert_eq!(uf.switch_method().unwrap(), UnitSwitchMethod::Reload);
    }

    #[test]
    fn can_handle_legacy_restart() {
        let uf = UnitFile::load(&test_data_path("legacy-restart.service")).unwrap();
        assert_eq!(uf.switch_method().unwrap(), UnitSwitchMethod::Restart);
    }

    #[test]
    fn can_handle_legacy_stop_start() {
        let uf = UnitFile::load(&test_data_path("legacy-stop-start.service")).unwrap();
        assert_eq!(uf.switch_method().unwrap(), UnitSwitchMethod::StopStart);
    }

    #[test]
    fn can_handle_legacy_keep_old() {
        let uf = UnitFile::load(&test_data_path("legacy-keep-old.service")).unwrap();
        assert_eq!(uf.switch_method().unwrap(), UnitSwitchMethod::KeepOld);
    }

    #[test]
    fn switch_method_defaults_to_stop_start() {
        let uf = UnitFile::load(&test_data_path("empty.service")).unwrap();
        assert_eq!(uf.switch_method().unwrap(), UnitSwitchMethod::StopStart);
    }

    #[test]
    fn can_add_option_using_overrides() {
        let uf = UnitFile::load(&test_data_path("unit_with_override.service")).unwrap();
        assert_eq!(uf.switch_method().unwrap(), UnitSwitchMethod::Reload);
    }

    #[test]
    fn can_override_option_using_overrides() {
        let uf = UnitFile::load(&test_data_path("unit_with_override.service")).unwrap();
        assert_eq!(uf.refuse_manual_start().unwrap(), true);
    }
}
