use std::{
    env,
    path::{Path, PathBuf},
    process::exit,
    time::Duration,
};

const PKG_NAME: Option<&'static str> = option_env!("CARGO_PKG_NAME");
const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");

#[derive(Debug)]
pub struct Options {
    /// Whether to log, but not perform, actions.
    pub dry_run: bool,

    /// Whether to log all actions taken.
    pub verbose: bool,

    /// How long to wait for units to start, stop, reload, etc. If none then
    /// timeout never happens.
    pub timeout: Option<Duration>,

    /// Path to the directory of old unit files, if any exists.
    pub old_dir: Option<String>,

    /// Path to the directory of new unit files.
    pub new_dir: String,
}

/// Parses the given command line arguments.
pub fn parse_args(args: &[String]) -> Options {
    let program = args[0].clone();

    let mut opts = getopts::Options::new();
    opts.optopt(
        "",
        "new-units",
        "Path to target directory of systemd unit files.",
        "DIR",
    )
    .optopt(
        "",
        "old-units",
        "Path to reference directory of systemd unit files.",
        "DIR",
    )
    .optflag(
        "n",
        "dry-run",
        "Print, but do not perform, the switch actions.",
    )
    .optopt(
        "",
        "timeout",
        "The number of milliseconds to wait for units to start, stop, \
         reload, etc.",
        "MS",
    )
    .optflag("v", "verbose", "Log all actions.")
    .optflag("h", "help", "Print command help.")
    .optflag("V", "version", "Print the program version.");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => {
            eprintln!("{}", f.to_string());
            eprintln!("Run '{} --help' for help.", program);
            exit(1);
        }
    };

    if matches.opt_present("help") {
        print_usage(&program, &opts);
        exit(0);
    }

    if matches.opt_present("version") {
        println!("{}", package_version());
        exit(0);
    }

    let timeout = match matches.opt_str("timeout").map(|s| s.parse()) {
        Some(Ok(millis)) if millis > 0 => Some(Duration::from_millis(millis)),
        Some(_) => {
            eprintln!("Invalid timeout value, expected positive whole number.");
            exit(1);
        }
        None => None,
    };

    if !matches.opt_present("new-units") {
        eprintln!("Required option 'new-units' missing.");
        eprintln!("Run '{} --help' for help.", program);
        exit(1);
    }

    Options {
        dry_run: matches.opt_present("dry-run"),
        verbose: matches.opt_present("verbose"),
        timeout,
        old_dir: matches.opt_str("old-units"),
        new_dir: matches.opt_str("new-units").unwrap(),
    }
}

fn package_version() -> String {
    format!(
        "{} {}",
        PKG_NAME.unwrap_or("[unknown package]"),
        VERSION.unwrap_or("[unknown version]")
    )
}

fn print_usage(program: &str, opts: &getopts::Options) {
    let brief = format!(
        "{}\n\
         \n\
         Given two directories of systemd unit files, this tool will stop, start,\n\
         restart, etc. services in the first directory to match the units in the\n\
         second directory.\n\
         \n\
         Usage: {} [options]",
        package_version(),
        program
    );
    print!("{}", opts.usage(&brief));
}

fn main() {
    let options = parse_args(&env::args().collect::<Vec<_>>());

    if options.verbose {
        println!("Options are {:#?}", options);
    }

    if options.dry_run {
        println!("Performing dry-run");
    }

    if options.verbose {
        println!("Enabling verbose output");
    }

    sd_switch::switch(
        &options.old_dir.map(|e| PathBuf::from(&e)),
        Path::new(&options.new_dir),
        options.dry_run,
        options.verbose,
        &options.timeout,
    )
    .expect("Error switching")
}
