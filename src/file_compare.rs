use std::{fs, path::Path};

/// Determines whether the two given paths are files having the same
/// content.
pub fn file_eq<P: AsRef<Path>>(path1: P, path2: P) -> std::io::Result<bool> {
    if fs::metadata(&path1)?.len() != fs::metadata(&path2)?.len() {
        return Ok(false);
    }

    Ok(fs::read(&path1)? == fs::read(&path2)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::{io, path::Path};

    const PATH_ABC: &str = "testdata/file_compare/file_containing_abc";
    const PATH_ABCD: &str = "testdata/file_compare/file_containing_abcd";
    const PATH_XYZ: &str = "testdata/file_compare/file_containing_xyz";

    #[test]
    fn files_with_same_content_returns_true() {
        let actual = file_eq(Path::new(PATH_ABC), Path::new(PATH_ABC));
        assert!(actual.unwrap());
    }

    #[test]
    fn files_of_different_size_returns_false() {
        let actual = file_eq(Path::new(PATH_ABC), Path::new(PATH_ABCD));
        assert!(!actual.unwrap());
    }

    #[test]
    fn files_of_same_size_but_different_content_returns_false() {
        let actual = file_eq(Path::new(PATH_ABC), Path::new(PATH_XYZ));
        assert!(!actual.unwrap());
    }

    #[test]
    fn bad_path_returns_path_not_found() {
        let actual = file_eq(Path::new(PATH_ABC), Path::new("no-such-file"));
        assert_eq!(actual.unwrap_err().kind(), io::ErrorKind::NotFound);
    }
}
