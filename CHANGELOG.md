# 0.2.3 (2021-06-26)

* Use anyhow to add a bit more helpful error messages.

# 0.2.2 (2021-05-21)

* Fix bug where sd-switch would attempt to start a `target` unit even
  if it contains `RefuseManualStart=yes`.

# 0.2.1 (2021-04-20)

* Bump rust-ini to 0.17.

* Bump dbus to 0.9.

* A few minor improvements.

# 0.2.0 (2020-07-29)

* Add `--timeout` option that allows restricting the maximum time to
  wait for systemd jobs to finish.

# 0.1.0 (2020-07-18)

* The initial release of sd-switch, a program used by [Home Manager][]
  to start, stop, restart, etc. systemd units to match the setup of a
  new configuration generation.

[Home Manager]: https://github.com/rycee/home-manager
