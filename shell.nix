let sources = import ./nix/sources.nix;
in { pkgs ? import sources.nixpkgs { } }:

let

  package = import ./default.nix { inherit pkgs; };

in pkgs.mkShell {
  name = "nix-shell-for-sd-switch";
  nativeBuildInputs = with pkgs; [
    cargo-audit
    cargo-license
    clippy
    glib.bin
    niv
    nixfmt
    package.buildInputs
    package.nativeBuildInputs
    pkg-config
    rust-analyzer
    rustc
    rustfmt
  ];
}
